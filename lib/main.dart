import 'package:flutter/material.dart';
import 'view/smooth_slider.dart';

void main() =>
    runApp(MaterialApp(
      home: App(),
    ));

class App extends StatefulWidget {
  App({Key key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        body: Center(
            child: SmoothSlider(),
        ));
  }
}
