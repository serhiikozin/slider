class BadgeDefenitions {
  double radius;
  double progress;
  double minHeight;
  double minWeight;
  double topPosition;
  double bottomPosition;
  double leftPosition;
  double rightPosition;
  double leftControlPosition;
  double rightControlPosition;
  double halfHeight;

  BadgeDefenitions(
      this.radius,
      this.progress,
      this.halfHeight,
      this.minHeight,
      this.minWeight,
      this.topPosition,
      this.bottomPosition,
      this.leftPosition,
      this.rightPosition,
      this.leftControlPosition,
      this.rightControlPosition);
}
