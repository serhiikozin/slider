import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:slider/model/WaveSliderControler.dart';

import 'badge_slider_painter.dart';
import 'line_slider_painter.dart';

class SmoothSlider extends StatefulWidget {
  final double width;
  final double height;
  final Color color;

  const SmoothSlider(
      {Key key, this.width = 350, this.height = 30, this.color = Colors.grey})
      : super(key: key);

  @override
  _SmoothSliderState createState() => _SmoothSliderState();
}

class _SmoothSliderState extends State<SmoothSlider>
    with SingleTickerProviderStateMixin {
  double _dragPosition = 0;
  double _dragPercentage = 0;

  WaveSliderController _slideController;

  @override
  void initState() {
    super.initState();
    _slideController = WaveSliderController(vsync: this)
      ..addListener(() => setState(() {}));
  }

  @override
  void dispose() {
    super.dispose();
    _slideController.dispose();
  }

  void _updateDragPosition(Offset value) {
    double newDragPosition = 0;

    if (value.dx <= 0) {
      newDragPosition = 0;
    } else if (value.dx >= widget.width) {
      newDragPosition = widget.width;
    } else {
      newDragPosition = value.dx;
    }

    setState(() {
      _dragPosition = newDragPosition;
      _dragPercentage = _dragPosition / widget.width;
      print("Point Drag Percent (X): " + _dragPercentage.toString());
    });
  }

  void _onDragStart(BuildContext context, DragStartDetails start) {
    RenderBox box = context.findRenderObject();
    Offset offset = box.globalToLocal(start.globalPosition);
    _slideController.setStateToStart();
    _updateDragPosition(offset);
  }

  void _onDragUpdate(BuildContext context, DragUpdateDetails update) {
    RenderBox box = context.findRenderObject();
    Offset offset = box.globalToLocal(update.globalPosition);
    _updateDragPosition(offset);
  }

  void _onDragEnd(BuildContext context, DragEndDetails end) {
    _slideController.setStateToStopping();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Container(
            height: widget.height,
            width: widget.width,
            color: Colors.grey,
            child: CustomPaint(
              painter: BadgeSliderPainter(
                  _dragPosition, widget.color,
                  _slideController.progress, _slideController.state
              ),
            ),
          ),
          Container(
            height: widget.height,
            width: widget.width,
            child: CustomPaint(
              painter: LineSliderPainter(_dragPosition, widget.color,
                  _slideController.progress, _slideController.state),
            ),
          ),
        ]),
        onHorizontalDragUpdate: (DragUpdateDetails update) =>
            _onDragUpdate(context, update),
        onHorizontalDragStart: (DragStartDetails start) =>
            _onDragStart(context, start),
        onHorizontalDragEnd: (DragEndDetails end) => _onDragEnd(context, end),
      ),
    );
  }
}
