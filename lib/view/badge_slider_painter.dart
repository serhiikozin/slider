import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:slider/model/BadgeDefenitions.dart';
import 'package:slider/model/SliderState.dart';

class BadgeSliderPainter extends CustomPainter {
  final double sliderPosition;
  final double animationProgress;
  final SliderState state;
  final Color color;

  double _previewsSliderPosition = 0;

  final backgroundPaintBadge = Paint()
    ..color = Colors.black
    ..style = PaintingStyle.fill;

  BadgeSliderPainter(
      this.sliderPosition, this.color, this.animationProgress, this.state);

  @override
  void paint(Canvas canvas, Size size) {
    _paintBadgeBackground(canvas, size, sliderPosition);
  }

  @override
  bool shouldRepaint(BadgeSliderPainter oldDelegate) {
    _previewsSliderPosition = oldDelegate.sliderPosition;
    return true;
  }

  void _paintBadgeBackground(Canvas canvas, Size size, double centerPosition) {
    BadgeDefenitions defen =
        _calculateBadgetDefenition(size.height, size.width, centerPosition);

    Path path = Path()
      ..moveTo(defen.leftPosition + defen.radius, defen.topPosition)
      ..lineTo(defen.rightPosition - defen.radius, defen.topPosition)
      ..quadraticBezierTo(defen.rightPosition, defen.topPosition,
          defen.rightPosition, defen.topPosition + defen.radius)
      ..quadraticBezierTo(defen.rightControlPosition, defen.halfHeight,
          defen.rightPosition, defen.bottomPosition - defen.radius)
      ..quadraticBezierTo(defen.rightPosition, defen.bottomPosition,
          defen.rightPosition - defen.radius, defen.bottomPosition)
      ..lineTo(defen.leftPosition + defen.radius, defen.bottomPosition)
      ..quadraticBezierTo(defen.leftPosition, defen.bottomPosition,
          defen.leftPosition, defen.bottomPosition - defen.radius)
      ..quadraticBezierTo(defen.leftControlPosition, defen.halfHeight,
          defen.leftPosition, defen.topPosition + defen.radius)
      ..quadraticBezierTo(defen.leftPosition, defen.topPosition,
          defen.leftPosition + defen.radius, defen.topPosition);

    canvas.drawPath(path, backgroundPaintBadge);
  }

  BadgeDefenitions _calculateBadgetDefenition(
      double height, double width, double centerPosition) {
    final double maxProgress = 100;
    final double radius = 3.0;
    final halfHeight = height / 2;
    final heightToWidthRatio = 16 / 9;

    final progress = sliderPosition / width * maxProgress;

    final plusHeight = halfHeight / maxProgress * progress;

    final double minHeight = halfHeight + plusHeight / 2;
    final double minWeight = minHeight * heightToWidthRatio;

    double topPosition = halfHeight - minHeight / 2;
    double bottomPosition = halfHeight + minHeight / 2;

    double leftPosition = centerPosition - minWeight / 2;
    double rightPosition = centerPosition + minWeight / 2;

    double bendability = 5.0;
    double maxSlideDifference = 5.0;

    double slideDifference = (sliderPosition - _previewsSliderPosition).abs();

    slideDifference = (slideDifference > maxSlideDifference)
        ? maxSlideDifference
        : slideDifference;

    double bend =
        lerpDouble(0.0, bendability, slideDifference / maxSlideDifference);

    bool moveLeft = sliderPosition < _previewsSliderPosition;

    bend = moveLeft ? -bend : bend;

    final leftControlPosition = leftPosition - bend;
    final rightControlPosition = rightPosition - bend;

    return BadgeDefenitions(
        radius,
        progress,
        halfHeight,
        minHeight,
        minWeight,
        topPosition,
        bottomPosition,
        leftPosition,
        rightPosition,
        leftControlPosition,
        rightControlPosition);
  }
}
