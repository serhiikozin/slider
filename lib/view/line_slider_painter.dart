import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:slider/model/SliderState.dart';
import 'package:slider/model/LineDefenitions.dart';

class LineSliderPainter extends CustomPainter {
  final double sliderPosition;
  final double animationProgress;
  final SliderState sliderState;
  final Color color;

  double _previewsSliderPosition = 0;

  Paint paintCircle = Paint()
    ..color = Colors.black
    ..style = PaintingStyle.fill;

  Paint paintLine = Paint()
    ..color = Colors.black
    ..style = PaintingStyle.stroke
    ..strokeWidth = 2;

  LineSliderPainter(this.sliderPosition, this.color, this.animationProgress,
      this.sliderState){
    paintLine..color = color;
  }

  @override
  void paint(Canvas canvas, Size size) {
    double height = size.height / 2;
    double width = size.width;

    switch (sliderState) {
      case SliderState.starting:
        _paintStartupWave(canvas, height, width);
        break;
      case SliderState.resting:
        _paintRestingWave(canvas, height, width);
        break;
      case SliderState.sliding:
        _paintSlidingWave(canvas, height, width);
        break;
      case SliderState.stopping:
        _paintStoppingWave(canvas, height, width);
        break;
      default:
        _paintSlidingWave(canvas, height, width);
        break;
    }

    _paintDots(canvas, height, width, 5, paintCircle);
  }

  void _paintDots(
      Canvas canvas, double height, double width, double radius, Paint paint) {
    canvas.drawCircle(Offset(0, height), radius, paint);
    canvas.drawCircle(Offset(width, height), radius, paint);
  }

  void _paintMarker(Canvas canvas, Offset offset, double radius, paint) {
    canvas.drawCircle(offset, radius, paint);
  }

  void _paintWaveLine(
      Canvas canvas, double height, double width, LineCurveDefinitions wave) {
    Path path = Path()
      ..moveTo(0, height)
      ..lineTo(wave.startOfBezier, height)
      ..cubicTo(wave.leftControlPointOne, height, wave.leftControlPointTwo,
          wave.controlHeight, wave.centerPosition, wave.controlHeight)
      ..cubicTo(wave.rightControlPointOne, wave.controlHeight,
          wave.rightControlPointTwo, height, wave.endOfBezier, height)
      ..lineTo(width, height);

    canvas.drawPath(path, paintLine);
  }

  LineCurveDefinitions _calculateLineDefenitions(double height, double width) {
    double bendWidth = 20.0;
    double bezierWidth = 20.0;

    double controlHeight = 0.0;
    double centerPosition = sliderPosition;

    double startOfBend = sliderPosition - bendWidth / 2;
    double startOfBezier = startOfBend - bezierWidth;
    double endOfBend = sliderPosition + bendWidth / 2;
    double endOfBezier = endOfBend + bezierWidth;

    startOfBend = (startOfBend <= 0.0) ? 0.0 : startOfBend;
    startOfBezier = (startOfBezier <= 0.0) ? 0.0 : startOfBezier;
    endOfBend = (endOfBend > width) ? width : endOfBend;
    endOfBezier = (endOfBezier > width) ? width : endOfBezier;

    double leftControlPointOne = startOfBend;
    double leftControlPointTwo = startOfBend;

    double rightControlPointOne = endOfBend;
    double rightControlPointTwo = endOfBend;

    double bendability = 5.0;
    double maxSlideDifference = 5.0;

    double slideDifference = (sliderPosition - _previewsSliderPosition).abs();

    slideDifference = (slideDifference > maxSlideDifference)
        ? maxSlideDifference
        : slideDifference;

    double bend =
        lerpDouble(0.0, bendability, slideDifference / maxSlideDifference);

    bool moveLeft = sliderPosition < _previewsSliderPosition;

    bend = moveLeft ? -bend : bend;

    leftControlPointOne += bend;
    leftControlPointTwo += -bend;
    rightControlPointOne += -bend;
    rightControlPointTwo += bend;

    LineCurveDefinitions wave = LineCurveDefinitions(
        controlHeight,
        centerPosition,
        startOfBend,
        startOfBezier,
        endOfBend,
        endOfBezier,
        leftControlPointOne,
        leftControlPointTwo,
        rightControlPointOne,
        rightControlPointTwo);
    return wave;
  }

  _paintStartupWave(Canvas canvas, double height, double width) {
    LineCurveDefinitions line = _calculateLineDefenitions(height, width);

    double lineHeight =
        _lerpLine(height, line.controlHeight, animationProgress);

    line.controlHeight = lineHeight;

    _paintWaveLine(canvas, height, width, line);
    _paintMarker(canvas, Offset(sliderPosition, height / 2 + lineHeight), 5,
        paintCircle);
  }

  _paintRestingWave(Canvas canvas, double height, double width) {
    Path path = Path();
    path.moveTo(0.0, height);
    path.lineTo(width, height);
    canvas.drawPath(path, paintLine);

    _paintMarker(canvas, Offset(sliderPosition, 2 * height), 5, paintCircle);
  }

  _paintSlidingWave(Canvas canvas, double height, double width) {
    LineCurveDefinitions line = _calculateLineDefenitions(height, width);
    _paintWaveLine(canvas, height, width, line);
  }

  _paintStoppingWave(Canvas canvas, double height, double width) {
    LineCurveDefinitions line = _calculateLineDefenitions(height, width);

    double lineHeight =
        _lerpLine(line.controlHeight, height, animationProgress);

    line.controlHeight = lineHeight;

    _paintWaveLine(canvas, height, width, line);
    _paintMarker(
        canvas, Offset(sliderPosition, height + lineHeight), 5, paintCircle);
  }

  double _lerpLine(double heightStart, double heightEnd, double progress) {
    return lerpDouble(
        heightStart, heightEnd, Curves.elasticOut.transform(progress));
  }

  @override
  bool shouldRepaint(LineSliderPainter oldDelegate) {
    _previewsSliderPosition = oldDelegate.sliderPosition;
    return true;
  }
}
